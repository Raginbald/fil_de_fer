# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/16 13:48:28 by graybaud          #+#    #+#              #
#    Updated: 2015/02/12 17:36:43 by graybaud         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		= fdf
DEBUG		= debug_version_$(NAME)
LIBFT_DIR	= libft
LIBFT		= $(LIBFT_DIR)/libft.a
CC			= gcc
LFLAGS		= -L ./libft -lft -L /usr/x11/lib -lmlx -lX11 -lXext
CFLAGS		= -Wall -Wextra -Werror -I ./libft/ -I /usr/include -I./
DFLAGS		= -g -DD_ERROR_ON
RM			= rm -Rf
H			= 	fdf.h 			\
				struct.h 		\
				define.h

OBJ			= $(SRC:.c=.o)
SRC			= 	main.c				\
				ft_init.c			\
				ft_init_env.c		\
				ft_init_counter.c	\
				ft_check_init.c 	\
				ft_open_close.c		\
				ft_get_data.c		\
				ft_exit.c 			\
				ft_press_release.c 	\
				ft_loop_hook.c 		\
				ft_expose.c 		\
				ft_pixel_put.c 		\
				ft_key_zoom.c 		\
				ft_launch_window.c 	\
				ft_projection.c 	\
				ft_key_control.c 	\
				ft_key_udrl.c 		\
				ft_draw_line.c

OBJ			= $(SRC:.c=.o)

$(NAME): $(LIBFT) $(OBJ) $(H)
	@echo "building $(NAME) ... "
	@$(CC) $(CFLAGS) -o $@ $(OBJ) $(LFLAGS)
	@echo "$(NAME) created ! \n"
	@echo "building $(DEBUG) ... "
	@$(CC) $(CFLAGS) $(DFLAGS) $(SRC) -o $(DEBUG) $(LFLAGS)
	@echo "$(DEBUG) created !"

$(LIBFT):
	@echo "building $(LIBFT_DIR) ... "
	@(cd  $(LIBFT_DIR) && $(MAKE))
	@echo "$(LIBFT_DIR) created ! \n"

%.o: %.c
	@$(CC) $(CFLAGS) -c $^ -o $@

all:$(NAME)

clean:
	@$(RM) $(DEBUG) $(DEBUG).dSYM $(OBJ)
	@(cd  $(LIBFT_DIR) && $(MAKE) $@)
	@echo "$(DEBUG), $(DEBUG).dSYM, objects erased !"

fclean:clean
	@$(RM) $(NAME)
	@(cd  $(LIBFT_DIR) && $(MAKE) $@)
	@echo "$(NAME), $(LIBFT_DIR) erased !\n"

re: fclean all

.PHONY:all clean fclean re
