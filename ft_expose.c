/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_expose.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/18 17:39:56 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/10 12:36:43 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include "define.h"
#include "fdf.h"

static void	ft_draw_map(t_env *env, t_pt **tab)
{
	t_pt	i;

	ft_init_counter(&i, -1);
	while (++I < LINE)
	{
		J = -1;
		while (++J < COL - 1)
			ft_draw_line_b_v_h(env, tab[I][J], tab[I][J + 1]);
	}
	ft_init_counter(&i, -1);
	while (++I < LINE - 1)
	{
		J = -1;
		while (++J < COL)
			ft_draw_line_b_v_h(env, tab[I][J], tab[I + 1][J]);
	}
}

void		ft_expose(t_env *env)
{
	IMG = mlx_new_image(INI, WIDTH, HEIGHT);
	IMG_PTR = mlx_get_data_addr(IMG, &BPP, &SIZELINE, &ENDIAN);
	ft_init_projection(env);
	if (FLAG == 'i')
		ft_draw_map(env, env->iso);
	if (FLAG == 'p')
		ft_draw_map(env, env->para);
	mlx_put_image_to_window(INI, WIN, IMG, 0, 0);
	mlx_destroy_image(INI, IMG);
}
