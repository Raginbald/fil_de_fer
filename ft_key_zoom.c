/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_key_zoom.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/07 17:04:56 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/09 16:54:22 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "define.h"
#include "fdf.h"

void		ft_key_zoom_out(t_env *env)
{
	M_SPACE -= 10;
	M_Z_SPACE -= 0.4;
	if (M_SPACE <= 10 || M_Z_SPACE <= 0.5)
	{
		M_SPACE = 10;
		M_Z_SPACE = 0.4;
	}
}

void		ft_key_zoom_in(t_env *env)
{
	M_SPACE += 10;
	M_Z_SPACE += 0.4;
	if (M_SPACE > 1000 || M_Z_SPACE > 100)
	{
		M_SPACE = 1000;
		M_Z_SPACE -= 0.4;
	}
}
