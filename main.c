/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/16 15:50:37 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/12 17:36:17 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <libft.h>
#include <stdlib.h>
#include "define.h"
#include "fdf.h"

int		main(int ac, char **av)
{
	t_env	*env;

	env = NULL;
	if (ac != 2)
		ft_exit(env, EXIT_FAILURE, 2, BAD_ARGS);
	else
	{
		env = ft_initialization(av[1]);
		if (!env)
			ft_exit(env, EXIT_FAILURE, 2, BAD_MAPS);
		FLAG = 'i';
		ft_launch_window(env);
	}
	return (0);
}
