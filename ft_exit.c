/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/31 15:41:52 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/12 17:51:01 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <stdlib.h>
#include <libft.h>
#include "define.h"
#include "fdf.h"

void	ft_end(char *str, int fd, int flag)
{
	ft_putendl_fd(str, fd);
	exit(flag);
}

void	ft_end_env(t_env *env, char *str, int fd, int flag)
{
	if (ft_check_init(env))
	{
		free(env);
		ft_end(str, fd, flag);
	}
	else
	{
		ft_free_tab(COORD);
		ft_free_tab(ISO);
		ft_free_tab(PARA);
		free(env);
		ft_end(str, fd, flag);
	}
}

void	ft_end_env_mlx(t_env *env, char *str, int fd, int flag)
{
	mlx_clear_window(INI, WIN);
	mlx_destroy_window(INI, WIN);
	ft_end_env(env, str, fd, flag);
}

void	ft_free_tab(t_pt **tab)
{
	free(tab[0]);
	free(tab);
	tab = NULL;
}

void	ft_exit(t_env *env, int flag, int fd, char *exit_str)
{
	if (!env)
		ft_end(exit_str, fd, flag);
	if (flag)
		ft_end_env(env, exit_str, fd, flag);
	else
		ft_end_env_mlx(env, exit_str, fd, flag);
}
