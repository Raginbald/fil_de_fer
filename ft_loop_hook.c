/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_loop_hook.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/31 18:17:35 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/10 12:22:49 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "define.h"
#include "fdf.h"

int		ft_loop_hook(t_env *env)
{
	if (END)
		ft_key_esc(env);
	if (MOVE_UP)
		ft_key_up(env);
	if (MOVE_DOWN)
		ft_key_down(env);
	if (MOVE_RIGHT)
		ft_key_right(env);
	if (MOVE_LEFT)
		ft_key_left(env);
	if (ZOOM_IN)
		ft_key_zoom_in(env);
	if (ZOOM_OUT)
		ft_key_zoom_out(env);
	if (DEFAULT)
		ft_key_deflt(env);
	if (PRO_ISO && (FLAG = 'i'))
		ft_key_deflt(env);
	if (PRO_PARA && (FLAG = 'p'))
		ft_key_deflt(env);
	ft_expose(env);
	return (0);
}
