/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_env.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/31 16:01:18 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/10 12:39:03 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "define.h"
#include "fdf.h"

t_mlx	*ft_init_mlx(void)
{
	t_mlx	*mlx;

	if (!(mlx = (t_mlx *)malloc(sizeof(t_mlx))))
		return (NULL);
	return (mlx);
}

t_pt	*ft_init_info(void)
{
	t_pt	*info;

	if (!(info = (t_pt *)malloc(sizeof(t_pt))))
		return (NULL);
	return (info);
}

t_pt	*ft_init_pixel(void)
{
	t_pt	*pixel;

	if (!(pixel = (t_pt *)malloc(sizeof(t_pt))))
		return (NULL);
	return (pixel);
}

t_key	*ft_init_key(void)
{
	t_key	*key;

	if (!(key = (t_key *)malloc(sizeof(t_key))))
		return (NULL);
	return (key);
}

t_pt	**ft_init_tab_pt(t_pt *info)
{
	t_pt	**tmp;
	t_pt	*tmp2;
	int		i;

	i = -1;
	if (!(tmp = (t_pt **)malloc(sizeof(t_pt *) * info->x)))
		return (NULL);
	if (!(tmp2 = (t_pt *)malloc(sizeof(t_pt) * info->z)))
		return (NULL);
	while (++i < info->x)
		tmp[i] = &tmp2[i * info->y];
	return (tmp);
}
