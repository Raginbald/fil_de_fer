/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_key_udrl.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/07 17:16:03 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/09 16:26:43 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "define.h"
#include "fdf.h"

void		ft_key_up(t_env *env)
{
	M_MV_Y -= 3;
}

void		ft_key_down(t_env *env)
{
	M_MV_Y += 3;
}

void		ft_key_left(t_env *env)
{
	M_MV_X -= 3;
}

void		ft_key_right(t_env *env)
{
	M_MV_X += 3;
}
