/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/30 19:04:58 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/12 17:56:52 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include "define.h"
#include "fdf.h"

static int		ft_check_file(char *file)
{
	if (ft_strstr(file, ".fdf"))
		return (0);
	else
		return (1);
}

static t_move	*ft_init_move(void)
{
	t_move	*tmp;

	if (!(tmp = (t_move *)malloc(sizeof(t_move))))
		return (NULL);
	return (tmp);
}

static void		ft_init_env_part(t_env *env)
{
	IMG_PTR = NULL;
	MLX = ft_init_mlx();
	INFO = ft_init_info();
	PIXEL = ft_init_pixel();
	KEY = ft_init_key();
	MOVE = ft_init_move();
}

t_env			*ft_initialization(char *file)
{
	t_env	*env;
	int		fd;

	fd = -1;
	env = NULL;
	if (ft_check_file(file))
		ft_exit(env, EXIT_FAILURE, 2, WRONG);
	if (!(env = (t_env *)malloc(sizeof(t_env))))
		ft_exit(env, EXIT_FAILURE, 2, BAD_ALLOC);
	ft_init_env_part(env);
	ft_check_init(env);
	fd = ft_check_open(env, file, fd);
	if (ft_get_info_file(env, fd))
		ft_exit(env, EXIT_FAILURE, 2, BAD_MAPS);
	ft_check_close(env, fd);
	COORD = ft_init_tab_pt(INFO);
	PARA = ft_init_tab_pt(INFO);
	ISO = ft_init_tab_pt(INFO);
	ft_check_tabs(env);
	fd = ft_check_open(env, file, fd);
	ft_get_coord(env, fd);
	ft_check_close(env, fd);
	ft_init_projection(env);
	return (env);
}
