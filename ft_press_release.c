/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_press_release.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/31 18:16:41 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/12 17:34:18 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "define.h"
#include "fdf.h"

int		ft_key_press(int key, t_env *env)
{
	if (key == ESC)
		END = 1;
	if (key == UP)
		MOVE_UP = 1;
	if (key == DOWN)
		MOVE_DOWN = 1;
	if (key == RIGHT)
		MOVE_RIGHT = 1;
	if (key == LEFT)
		MOVE_LEFT = 1;
	if (key == PLUS)
		ZOOM_IN = 1;
	if (key == MINUS)
		ZOOM_OUT = 1;
	if (key == ENTER_N)
		DEFAULT = 1;
	if (key == _I)
		PRO_ISO = 1;
	if (key == _P)
		PRO_PARA = 1;
	return (0);
}

int		ft_key_release(int key, t_env *env)
{
	if (key == ESC)
		END = 0;
	if (key == UP)
		MOVE_UP = 0;
	if (key == DOWN)
		MOVE_DOWN = 0;
	if (key == RIGHT)
		MOVE_RIGHT = 0;
	if (key == LEFT)
		MOVE_LEFT = 0;
	if (key == PLUS)
		ZOOM_IN = 0;
	if (key == MINUS)
		ZOOM_OUT = 0;
	if (key == ENTER_N)
		DEFAULT = 0;
	if (key == _I)
		PRO_ISO = 0;
	if (key == _P)
		PRO_PARA = 0;
	return (0);
}
