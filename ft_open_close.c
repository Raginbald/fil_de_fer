/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_open_close.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/18 20:29:35 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/10 12:50:40 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <libft.h>
#include "define.h"
#include "fdf.h"

void	ft_check_close(t_env *env, int fd)
{
	if (close(fd) == -1)
		ft_exit(env, EXIT_FAILURE, 2, CLOSE_FAILED);
}

int		ft_check_open(t_env *env, char *path, int fd)
{
	fd = open(path, O_RDONLY);
	if (fd == -1)
		ft_exit(env, EXIT_FAILURE, 2, OPEN_FAILED);
	return (fd);
}
