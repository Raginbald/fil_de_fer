/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_data.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/18 19:20:01 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/12 17:45:54 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <stdlib.h>
#include "define.h"
#include "fdf.h"

int			ft_get_info_file(t_env *env, int fd)
{
	char	*line;
	char	**tmp;
	int		ret;

	line = NULL;
	if ((ret = ft_get_next_line(fd, &line)) == -1)
		return (1);
	LINE++;
	if (!(tmp = ft_strsplit(line, ' ')))
		return (1);
	while (tmp[COL])
		COL++;
	while ((ret = ft_get_next_line(fd, &line)))
	{
		if (ret)
		{
			ft_strdel(&line);
			LINE++;
		}
	}
	ft_strdel(tmp);
	ft_strdel(&line);
	PTS = COL * LINE;
	return (0);
}

void		ft_get_coord(t_env *env, int fd)
{
	char	*line;
	char	**tmp;
	t_pt	i;

	line = NULL;
	tmp = NULL;
	ft_init_counter(&i, 0);
	while (ft_get_next_line(fd, &line))
	{
		tmp = ft_strsplit(line, 32);
		J = 0;
		while (J < COL)
		{
			COORD[I][J].z = ft_atoi(tmp[J]);
			COORD[I][J].x = J;
			COORD[I][J].y = I;
			J++;
		}
		I++;
		ft_strdel(tmp);
		ft_strdel(&line);
	}
	ft_strdel(&line);
}
