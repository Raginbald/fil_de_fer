/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/16 20:08:46 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/12 17:40:51 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "struct.h"

t_env	*ft_initialization(char *file);
void	ft_init_counter(t_pt *i, int n);
t_mlx	*ft_init_mlx(void);
t_pt	*ft_init_info(void);
t_pt	*ft_init_pixel(void);
t_pt	**ft_init_tab_pt(t_pt *info);
t_key	*ft_init_key(void);
int		ft_check_init(t_env *env);
int		ft_check_tabs(t_env *env);
void	ft_check_close(t_env *env, int fd);
int		ft_check_open(t_env *env, char *path, int fd);
int		ft_get_info_file(t_env *env, int fd);
void	ft_get_coord(t_env *env, int fd);
void	ft_exit(t_env *env, int flag, int fd, char *exit_str);
void	ft_launch_window(t_env *env);
int		ft_loop_hook(t_env *env);
int		ft_key_press(int key, t_env *env);
int		ft_key_release(int key, t_env *env);
void	ft_expose(t_env *env);
void	ft_draw_line_b_v_h(t_env *env, t_pt p0, t_pt p1);
void	ft_pixel_put(t_env *env, int x, int y, int color);
void	ft_init_projection(t_env *env);
void	ft_key_esc(t_env *env);
void	ft_key_up(t_env *env);
void	ft_key_down(t_env *env);
void	ft_key_left(t_env *env);
void	ft_key_right(t_env *env);
void	ft_key_zoom_in(t_env *env);
void	ft_key_zoom_out(t_env *env);
void	ft_key_deflt(t_env *env);
void	ft_free_tab(t_pt **tab);

#endif
