/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_init.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/01 14:24:34 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/12 17:35:43 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "define.h"
#include "fdf.h"

int		ft_check_tabs(t_env *env)
{
	if (COORD && PARA && ISO)
		return (1);
	else
	{
		ft_exit(env, EXIT_FAILURE, 2, ERROR_COORD);
		return (0);
	}
}

int		ft_check_init(t_env *env)
{
	if (env && MLX && PIXEL && INFO && KEY && MOVE)
		return (1);
	else
	{
		ft_exit(env, EXIT_FAILURE, 2, ERROR_INIT);
		return (0);
	}
}
