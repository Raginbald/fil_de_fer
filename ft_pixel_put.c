/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pixel_put.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/03 12:55:38 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/12 17:39:22 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include "define.h"
#include "fdf.h"

void			ft_pixel_put(t_env *env, int x, int y, int color)
{
	int		res;

	res = x * BPP / 8 + y * SIZELINE;
	if (x < WIDTH && y < HEIGHT && x > 0 && y > 0 && res > 0)
	{
		if (ENDIAN)
		{
			IMG_PTR[res] = color;
			IMG_PTR[res + 1] = color >> 8;
			IMG_PTR[res + 2] = color >> 16;
		}
		else
		{
			IMG_PTR[res] = color >> 16;
			IMG_PTR[res + 1] = color >> 8;
			IMG_PTR[res + 2] = color;
		}
	}
}
