/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_projection.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/04 16:18:06 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/12 17:32:45 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "define.h"
#include "fdf.h"

static t_pt	ft_topara(t_env *env, const t_pt *pt)
{
	t_pt	para;
	t_pt	tmp;

	tmp.x = pt->x * M_SPACE;
	tmp.y = pt->y * M_SPACE;
	tmp.z = pt->z * M_Z_SPACE;
	para.x = tmp.x - M_CTE * tmp.z;
	para.y = tmp.y - M_CTE / 2 * tmp.z;
	para.x += M_MV_X;
	para.y += M_MV_Y;
	return (para);
}

static t_pt	ft_toiso(t_env *env, const t_pt *pt)
{
	t_pt	iso;
	t_pt	tmp;

	tmp.x = M_CTE * pt->x * M_SPACE;
	tmp.y = M_CTE2 * pt->y * M_SPACE;
	tmp.z = pt->z * M_Z_SPACE;
	iso.x = tmp.x - tmp.y + M_MV_X;
	iso.y = tmp.x / 2 + tmp.y / 2 - tmp.z + M_MV_Y;
	return (iso);
}

void		ft_init_projection(t_env *env)
{
	t_pt	i;

	ft_init_counter(&i, -1);
	while (++I < LINE)
	{
		J = -1;
		while (++J < COL)
		{
			env->iso[I][J] = ft_toiso(env, &env->coord[I][J]);
			env->para[I][J] = ft_topara(env, &env->coord[I][J]);
		}
	}
}
