/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_line.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/18 17:21:42 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/12 17:45:43 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <libft.h>
#include "define.h"
#include "fdf.h"

static void		ft_draw_line_v(t_env *env, t_pt p0, t_pt p1)
{
	int	incr;

	incr = p0.y < p1.y ? 1 : -1;
	while (p0.y != p1.y)
	{
		ft_pixel_put(env, X0, Y0, 0xFF0000);
		p0.y += incr;
	}
}

static void		ft_draw_line_h(t_env *env, t_pt p0, t_pt p1)
{
	int	incr;

	incr = p0.x < p1.x ? 1 : -1;
	while (p0.x != p1.x)
	{
		ft_pixel_put(env, X0, Y0, 0xFF0000);
		p0.x += incr;
	}
}

static void		ft_draw_line_b(t_env *env, t_pt p0, t_pt p1)
{
	t_pt		d;
	t_pt		s;
	t_pt		e;

	d.x = ft_abs(X1 - X0);
	d.y = ft_abs(Y1 - Y0);
	s.x = X0 < X1 ? 1 : -1;
	s.y = Y0 < Y1 ? 1 : -1;
	e.x = (d.x > d.y ? d.x : -d.y);
	while (X0 != X1 && Y0 != Y1)
	{
		ft_pixel_put(env, X0, Y0, 0xFF0000);
		e.y = e.x * 2;
		if (e.y > -d.x)
		{
			e.x -= d.y;
			X0 += s.x;
		}
		if (e.y < d.y)
		{
			e.x += d.x;
			Y0 += s.y;
		}
	}
	ft_pixel_put(env, X0, Y0, 0xFF0000);
}

void			ft_draw_line_b_v_h(t_env *env, t_pt p0, t_pt p1)
{
	if (p0.x == p1.x)
		ft_draw_line_v(env, p0, p1);
	if (p0.y == p1.y)
		ft_draw_line_h(env, p0, p1);
	if (p0.x != p1.x && p0.y != p1.y)
		ft_draw_line_b(env, p0, p1);
}
