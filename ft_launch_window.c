/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_launch_window.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/31 18:18:16 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/10 12:48:49 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include "define.h"
#include "fdf.h"

static void		ft_init_window(t_env *env)
{
	WIDTH = SPACE * 2 + COL * SPACE;
	WIDTH = WIDTH < 600 ? 600 : WIDTH;
	WIDTH = WIDTH > 1860 ? 1860 : WIDTH;
	HEIGHT = SPACE * 5 + LINE * SPACE;
	HEIGHT = HEIGHT < 400 ? 400 : HEIGHT;
	HEIGHT = HEIGHT > 1240 ? 1240 : HEIGHT;
}

void			ft_launch_window(t_env *env)
{
	INI = mlx_init();
	ft_init_window(env);
	ft_key_deflt(env);
	WIN = mlx_new_window(INI, WIDTH, HEIGHT, "FDF");
	mlx_hook(WIN, 2, 1, ft_key_press, env);
	mlx_hook(WIN, 3, 2, ft_key_release, env);
	mlx_loop_hook(INI, ft_loop_hook, env);
	mlx_loop(INI);
}
