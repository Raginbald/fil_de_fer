/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/30 19:12:51 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/12 17:46:37 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCT_H
# define STRUCT_H

typedef struct s_env	t_env;
typedef struct s_mlx	t_mlx;
typedef struct s_pt		t_pt;
typedef struct s_key	t_key;
typedef struct s_move	t_move;

struct			s_mlx
{
	void		*ini;
	void		*win;
	void		*img;
};

struct			s_pt
{
	int			x;
	int			y;
	int			z;
};

struct			s_move
{
	double		mv_x;
	double		mv_y;
	int			space;
	double		z_space;
	double		cte;
	double		cte2;
};

struct			s_key
{
	char		end;
	char		move_up;
	char		move_down;
	char		move_left;
	char		move_right;
	char		zoom_in;
	char		zoom_out;
	char		deflt;
	char		cte;
	char		cte2;
	char		space;
	char		pro_iso;
	char		pro_para;
};

struct			s_env
{
	t_mlx		*mlx;
	int			width;
	int			height;
	t_pt		*pixel;
	t_key		*key;
	t_move		*move;
	char		*img_ptr;
	int			bpp;
	int			sizeline;
	int			endian;
	t_pt		**coord;
	t_pt		**iso;
	t_pt		**para;
	t_pt		*info;
	char		flag;
};

#endif
