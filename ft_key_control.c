/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_key_control.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/30 18:50:02 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/11 14:56:09 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "define.h"
#include "fdf.h"

void	ft_key_esc(t_env *env)
{
	if (env)
		ft_exit(env, EXIT_SUCCESS, 1, PROG_EXIT);
	else
		ft_exit(env, EXIT_FAILURE, 2, PROG_EXIT_FAIL);
}

void	ft_key_deflt(t_env *env)
{
	if (FLAG == 'i')
	{
		M_MV_X = WIDTH / 2;
		M_MV_Y = HEIGHT / 3;
		M_SPACE = SPACE;
		M_Z_SPACE = 2;
		M_CTE = CTE;
		M_CTE2 = CTE2;
	}
	if (FLAG == 'p')
	{
		M_MV_X = WIDTH / 7;
		M_MV_Y = HEIGHT / 6;
		M_SPACE = SPACE;
		M_Z_SPACE = 2;
		M_CTE = CTE;
		M_CTE2 = CTE2;
	}
}
