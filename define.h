/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   define.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/30 19:13:05 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/12 17:57:37 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEFINE_H
# define DEFINE_H

# include "struct.h"

# define CTE 			0.75
# define CTE2			0.75
# define RIGHT			65363
# define LEFT			65361
# define UP				65362
# define DOWN			65364
# define ESC			65307
# define PLUS			65451
# define MINUS			65453
# define ENTER_N		65421
# define _K				107
# define _L				108
# define _I				105
# define _P				112
# define SPACE			42
# define WRONG			"Error: Wrong map !"
# define BAD_MAPS		"Error: Bad map !"
# define BAD_ALLOC		"Error: can't allocate memory !"
# define BAD_ARGS		"Error: Bad argument !\nUsage: ./fdf map"
# define ERROR_INIT		"Error: Bad initialization, retry"
# define ERROR_COORD	"Error: Bad initialization of coords"
# define CLOSE_FAILED	"Error: close() failed"
# define OPEN_FAILED	"Error: open() failed"
# define PROG_EXIT		"program exit normaly, good bye"
# define PROG_EXIT_FAIL	"Error: program shutdown, you may have some leaks, oups"
# define MLX			env->mlx
# define PIXEL 			env->pixel
# define IMG_PTR 		env->img_ptr
# define COORD			env->coord
# define INFO			env->info
# define KEY			env->key
# define MOVE			env->move
# define ISO			env->iso
# define PARA			env->para
# define FLAG			env->flag
# define WIDTH 			env->width
# define HEIGHT			env->height
# define INI			env->mlx->ini
# define WIN			env->mlx->win
# define IMG			env->mlx->img
# define IMG_PTR 		env->img_ptr
# define BPP 			env->bpp
# define SIZELINE		env->sizeline
# define ENDIAN 		env->endian
# define END			env->key->end
# define MOVE_UP		env->key->move_up
# define MOVE_DOWN		env->key->move_down
# define MOVE_RIGHT		env->key->move_right
# define MOVE_LEFT		env->key->move_left
# define ZOOM_IN		env->key->zoom_in
# define ZOOM_OUT		env->key->zoom_out
# define DEFAULT		env->key->deflt
# define PRO_ISO		env->key->pro_iso
# define PRO_PARA		env->key->pro_para
# define M_MV_X			env->move->mv_x
# define M_MV_Y			env->move->mv_y
# define M_SPACE		env->move->space
# define M_Z_SPACE		env->move->z_space
# define M_CTE			env->move->cte
# define M_CTE2			env->move->cte2
# define X0				p0.x
# define Y0				p0.y
# define X1				p1.x
# define Y1				p1.y
# define I				i.x
# define J				i.y
# define LINE			env->info->x
# define COL			env->info->y
# define PTS			env->info->z

#endif
